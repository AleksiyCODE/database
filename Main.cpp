#include "single_include/nlohmann/json.hpp"
#include <iostream>
#include <iomanip> 
#include<Windows.h> 
#include <string>
#include <fstream> 
#include <sstream>

// create 
//_name_ 
// _fieldName_ _value_
// _fieldName_ _value_
// _fieldName_ _value_
//end

//add 
//_tableName_
//_objectID_ 
// _fieldName_ _value_
// _fieldName_ _value_
// _fieldName_ _value_
//end

//list
//_name_
//(_ID_)                //if '-1' then all 

//delete
//_name_
//(_ID_)                //if '-1' then all



#define STANDARD 0x0f
#define REVERSE 0xf0
using namespace nlohmann;
static const  HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

//template <typename ...Args>
//void PrintColoredText(const std::string& t, const Args &... args)
//{
//    methodB(t, f(t));
//    methodAHelper(args...);
//}


void PrintColoredText(std::string text)         //slow, but see if i care
{
    SetConsoleTextAttribute(hConsole, REVERSE);
    std::cout << text;
    SetConsoleTextAttribute(hConsole, STANDARD);
}

template <typename ...Args>
void PrintColoredText(std::string text, Args &&... args)
{
    PrintColoredText(text);
    PrintColoredText(args...);
}


void Create(json& j)
{    
    PrintColoredText("enter table name\n");
    std::string inp;
    std::cin >> inp;
    PrintColoredText(
        "now add fields\n",
        "to do it, enter lines in format: _fieldName_ _value_\n",
        "value can be int, float, bool or string\n",
        "if you want to enter string in value, use \"\"\n",
        "when you are done, write \"end\"\n"
    );

    std::string nam;
    std::string val;
    json k;
    for (;;)
    {
        std::cin >> nam;
        if (nam == "end")
           break;
        std::cin >> val;
        if (val[0] == '\"')                //entered string
        {
            k[nam] = val.substr(1,val.length()-2);
        }
        else if(val.find('.')!= std::string::npos)      //float
        {
            k[nam] = std::stof(val);
        }
        else if (val == "false")            //bool
        {
            k[nam] = false;
        }
        else if (val == "true")             //bool
        {
            k[nam] = true;
        }
        else                            //int
        {
            k[nam] = std::stoi(val);
        }
    }
    json t;
    t.emplace( "-1",k );
    j.emplace( inp,t );
    SetConsoleTextAttribute(hConsole, REVERSE);
    std::cout << "the layout of the table you just created is\n";
    std::cout << std::setw(4) << k<<std::endl;
    SetConsoleTextAttribute(hConsole, STANDARD);
}

void Add(json& j)
{
    PrintColoredText("enter table name\n");
    std::string target;
    std::cin >> target;
    PrintColoredText("enter ID of the object you are inserting\n");
    std::string id;
    std::cin >> id;
    json k;
    for (json::iterator it = j[target]["-1"].begin(); it != j[target]["-1"].end(); ++it)   
    {
        PrintColoredText("enter the value you want to put in field \"", it.key(),"\"\n");
        std::string val;
        std::cin >> val;
        if (val[0] == '\"')                //entered string
        {
            k[it.key()] = val.substr(1, val.length() - 2);
        }
        else if (val.find('.') != std::string::npos)      //float
        {
            k[it.key()] = std::stof(val);
        }
        else if (val == "false")            //bool
        {
            k[it.key()] = false;
        }
        else if (val == "true")             //bool
        {
            k[it.key()] = true;
        }
        else                            //int
        {
            k[it.key()] = std::stoi(val);
        }
    }
    j[target].emplace(id,k);
    SetConsoleTextAttribute(hConsole, REVERSE);
    std::cout << "the element you added looks like this\n";
    std::cout << std::setw(4) << k << std::endl;
    SetConsoleTextAttribute(hConsole, STANDARD);
}

void Delete(json& j)
{
    PrintColoredText("enter table name\n");
    std::string inp;
    std::cin >> inp;
    PrintColoredText(
        "enter ID of the object you want to delete\n",
        "enter \"-2\" if you want to delete the whole table\n");
    std::string id;
    std::cin >> id;
    if (id == "-2")
        j[inp].clear();
    else
    {
        j[inp].erase(id);
    }
        PrintColoredText("deleted sucessfully\n");
}

void List(json& j)
{
    PrintColoredText("enter table name\n");
    std::string inp;
    std::cin >> inp;
    PrintColoredText(
        "enter ID of the object you want to view\n",
        "enter \"-2\" if you want to view all entries\n");
    std::string id;
    std::cin >> id;
    SetConsoleTextAttribute(hConsole, REVERSE);
    if (id == "-2")
        std::cout << std::setw(4) << j[inp] << std::endl;
    else
        std::cout << std::setw(4) << j[inp][id] << std::endl;
    SetConsoleTextAttribute(hConsole, STANDARD);
}


int main()
{
    std::fstream fs;
    //SetConsoleTextAttribute(hConsole, k);
    /*
    * {"test":{"0":{"num":2345,"str":"word"}},"test2":{"0":{"size":10000,"temperature":36.599998474121094,"valid":true}}}
    * 
    * {"answer":{"everything":42},"happy":true,"list":[1,0,2],"name":"Niels","nothing":null,"object":{"currency":"USD","value":42.99},"pi":3.141}
        json j2 = {
        {"pi", 3.141},
        {"happy", true},
        {"name", "Niels"},
        {"nothing", nullptr},
        {"answer", {
          {"everything", 42}
        }},
        {"list", {1, 0, 2}},
        {"object", {
          {"currency", "USD"},
          {"value", 42.99}
        }}
        };

        std::string s = j2.dump();
        json j;
        std::cin >> j;

        // serialize to standard output
        std::cout << j;

        // the setw manipulator was overloaded to set the indentation for pretty printing
        std::cout << std::setw(4) << j << std::endl;
        // std::cout << s;



        {
         "number": 23,
         "string": "Hello, world!",
         "array": [1, 2, 3, 4, 5],
         "boolean": false,
         "null": null
}

{
         "number": 23,
         "string": "Hello, world!",
         "array": [1, 2, 3, 4, 5],
         "boolean": false,
         "null": null


         "initial":
{
    "0":
    {
            "number": 23,
            "string": "Hello, world!",
            "array": [1, 2, 3, 4, 5],
            "boolean": false,
            "null": null
    }
}

}
    */

    fs.open("DataBase.txt", std::fstream::in);
    json j;
    std::string inp;
    fs >> j;
    fs.close();
    for (;;)
    {
        PrintColoredText("crete, add, list, delete or exit? \n");

        std::cin >> inp;
        if (inp == "create")
        {
            Create(j);
        }
        else
            if (inp == "add")
            {
                Add(j);
            }
            else
                if (inp == "list")
                {
                    List(j);
                }
                else
                    if (inp == "delete")
                    {
                        Delete(j);
                    }
                    else
                        if (inp == "exit")
                            break;
                        else std::cout << "no such command \n";

    }

    //std::string s = j.dump();
    fs.open("DataBase.txt", std::fstream::out);
    fs<<j;
    fs.close();
	return 0;
}